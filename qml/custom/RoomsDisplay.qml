import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

SilicaListView {
    VerticalScrollDecorator {}

    id: list
    anchors.fill: parent

    property string type;

    spacing: Theme.paddingLarge
    clip: true

    currentIndex: -1
    property int savedY: 0

    function restoreAfterUpdate() {
        contentY = savedY
    }

    onDraggingChanged: {
        if (!dragging) {
            savedY = contentY
        }
    }


    onContentYChanged: {
        if (contentY != 0)
            savedY = contentY

    }

    onModelChanged: {
        contentY = savedY
    }

    PullDownMenu {
        MenuItem {
            text: qsTr("Settings")
            onClicked: pageStack.push("../pages/Settings.qml")
        }

        MenuItem {
            text:qsTr( "Explore public rooms")
            visible: type === "Rooms"
            onClicked: pageStack.push("../pages/RoomDirectory.qml")
        }

        MenuItem {
            text: qsTr("Create new room")
            visible: type === "Rooms"
            onClicked: pageStack.push("../pages/CreateRoom.qml");
        }

        MenuItem {
            text: headerItem.visible ? qsTr("Hide Search") : qsTr("Search")
            onClicked: {
                headerItem.visible = !headerItem.visible
                headerItem.height = headerItem.height == 0 ? Theme.itemSizeMedium : 0
                searchBar.text = "";
            }
        }
    }

    section.property: "section_name"
    section.criteria: ViewSection.FullString
    section.delegate: sectionHeading

    header: SearchField {
        width: parent.width
        placeholderText: qsTr("Search")
        id: searchBar
        visible: false
        height: 0
        onTextChanged: rooms.search(text)
    }

    onCountChanged: console.log("COUNT CHANGED: " + count)

    delegate: ListItem {
        id: item

        anchors.left: parent.left
        anchors.right: parent.right

        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin


        menu: ContextMenu {
            MenuItem {
                text: (section_name === "Favorites") ? qsTr("Remove from Favorites") : qsTr("Favorite")
                onClicked: {
                    if (section_name === "Favorites") {
                        rooms.unFavorite(rid)
                    } else {
                        rooms.setFavorite(rid)
                    }
                }

            }

            MenuItem {
                text: qsTr("Leave")
                onClicked: item.remorseDelete(function() { rooms.leave(rid); });
            }
        }


        onClicked: {
            rooms.mark_read(rid);
            active_room = rid;
            if (is_invite) {
                pageStack.push("../pages/Invite.qml", { name: name, room_id: rid, is_direct: is_direct});
            } else {
                pageStack.push("../pages/Messages.qml", { room_name: name, room_id: rid, roomIconPath: img.source});
            }
        }


        Image {
            id: img
            source: avatar ? avatar : ("image://theme/icon-m-chat?" + (pressed ? Theme.highlightColor : Theme.primaryColor))
            height: roomName.height + preview.height
            width: height

            fillMode: Image.PreserveAspectCrop

            layer.enabled: true
            layer.effect: OpacityMask {
                maskSource: Item {
                    width: roomName.height + preview.height
                    height: width
                    Rectangle {
                        radius: parent.width
                        anchors.centerIn: parent
                        width: parent.width
                        height: parent.height
                    }
                }
            }
        }

        Label {
            id: roomName
            text: unread > 0 ? "(" + unread + ") " + name : name
            anchors.left: img.right
            anchors.leftMargin: Theme.horizontalPageMargin
            truncationMode: TruncationMode.Fade
            font.pixelSize: Theme.fontSizeMedium
            width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
            font.bold: unread > 0
            color: Theme.primaryColor
        }
        Label {
            id: preview
            text: latestMessage
            anchors.top: roomName.bottom
            anchors.left: img.right
            anchors.right: parent.right
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin

            font.pixelSize: Theme.fontSizeExtraSmall
            truncationMode: TruncationMode.Fade
            width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
            color: Theme.secondaryColor
        }
    }
}
