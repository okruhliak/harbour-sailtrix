import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property string messageSource
    property string roomId
    property string eventId
    property var ts;


    allowedOrientations: Orientation.All

    PageHeader {
        title: qsTr("View Source")
        id: header
    }

    function formatTimestamp(timestamp) {
        var date = new Date(timestamp);

        return date.toLocaleString();
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: column.width
        contentHeight: column.height
        leftMargin: Theme.horizontalPageMargin
        rightMargin: Theme.horizontalPageMargin
        topMargin: header.height

        VerticalScrollDecorator{}
        HorizontalScrollDecorator{}

        Column {
            id: column

            Label {
                textFormat: Text.StyledText
                text: "Room ID: <pre>" + roomId + "</pre>"
                color: Theme.highlightColor
            }

            Label {
                textFormat: Text.StyledText
                text: "Event ID: <pre>" + eventId + "</pre>"
                color: Theme.highlightColor
            }

            Label {
                textFormat: Text.StyledText
                text: "Time: " + formatTimestamp(ts) + "<br>"
                color: Theme.highlightColor
            }

            Label {
                text: "Original event source:"
                width: page.width
                color: Theme.highlightColor
            }

            Label {
                font.family: "Monospace"
                text: messageSource
                textFormat: Text.PlainText
            }
        }
    }
}
